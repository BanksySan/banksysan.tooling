using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.FSharp.Core;
using Xunit;

namespace BanksySan.Tooling.FileSystemMonitor.Tests
{
    public class ArgumentParserTests
    {
        public static IEnumerable<object[]> InvalidPaths { get; } = new[]
        {
            new[] {Array.Empty<string>()},
            new[] { new[]  {""}},
            new[] {new[] {"Not a path <>"}},
            new[] {new[] {(string) null}}
        }.ToList();

        [Theory]
        [MemberData(nameof(InvalidPaths))]
        public void NoArgumentReturnsNone(string[] args)
        {
            var actual = ArgumentParser.parseArguments(args);
            Assert.Equal(FSharpOption<DirectoryInfo>.None, actual);
        }

        [Theory]
        [InlineData(@"\")]
        [InlineData(@"\c")]
        [InlineData(@"C:")]
        [InlineData(@"C:\")]
        [InlineData(@"Foo")]
        [InlineData(@"Foo\Bar")]
        [InlineData(@".\Foo")]
        [InlineData(@"..\Foo")]
        public void ValidPathsReturnSome(string arg)
        {
            var args = new[] {arg};
            var expected = new DirectoryInfo(arg);
            var actual = ArgumentParser.parseArguments(args);
            Assert.NotEqual(FSharpOption<DirectoryInfo>.None, actual);
            Assert.Equal(expected.FullName, actual.Value.FullName);
        }
    }
}
