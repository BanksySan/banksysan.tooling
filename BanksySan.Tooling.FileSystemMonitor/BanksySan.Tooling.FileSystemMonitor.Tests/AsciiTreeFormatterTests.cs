﻿using System;
using Microsoft.FSharp.Collections;
using Microsoft.FSharp.Core;
using Xunit;
using Xunit.Abstractions;
using static BanksySan.Tooling.AsciiTreeFormatter;

namespace BanksySan.Tooling.FileSystemMonitor.Tests
{
    public class AsciiTreeFormatterTests
    {
        public AsciiTreeFormatterTests(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }

        private readonly ITestOutputHelper _outputHelper;

        [Fact]
        public void FormatTree()
        {
            var node1 = new Node("node-1", FSharpList<Node>.Empty);
            var node2 = new Node("node-2", FSharpList<Node>.Empty);
            var node3 = new Node("node-3", FSharpList<Node>.Empty);
            var childNodes = new FSharpList<Node>(node1,
                new FSharpList<Node>(node2, new FSharpList<Node>(node3, FSharpList<Node>.Empty)));

            var node0 = new RootNode("node-0", childNodes);

            var expected =
                "node-0" + Environment.NewLine +
                "├node-1" + Environment.NewLine +
                "├node-2" + Environment.NewLine +
                "├node-3" + Environment.NewLine +
                "└node-4";

            var actual = Format(node0);
            _outputHelper.WriteLine($"Expected:\n{expected}");
            _outputHelper.WriteLine($"Formatted:\n{actual}");

            Assert.Equal(expected, actual);
        }
    }
}
