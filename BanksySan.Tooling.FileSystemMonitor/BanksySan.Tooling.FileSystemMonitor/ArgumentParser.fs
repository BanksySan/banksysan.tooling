﻿module ArgumentParser
open System.IO

let parseArguments arguments =
    let extractDirectory path =
        try 
            let directory = new DirectoryInfo(path) 
            Some directory
        with     
            | _ -> None
            
    let matchArguments arguments =
        match arguments with
        | [|path|] -> path |> extractDirectory
        | _ -> None

    arguments 
        |> matchArguments