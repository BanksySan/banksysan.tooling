﻿module BanksySan.Tooling.AsciiTreeFormatter

type Node = {
    Label: string;
    Children: Node list
}

type RootNode = {
    Label: string;
    Children: Node list
}

let Format rootNode =

    let formatMidNode node =
        node.Label
        |> sprintf "├%s"
    
    let formatEndNode node =
        node.Label 
        |> sprintf "└%s"

    let rec printNode level node  =
        let children = node.Children
        let label = node.Label
        let padding = String.replicate level " "
        
        0
    "--"