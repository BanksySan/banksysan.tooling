﻿module BanksySan.Tooling.PeDisassembler.Coff.Header

open System.IO
open System.Text
open BanksySan.Tooling.PeDisassembler

open Formatting

type Header = {
    Signature: Value<char list>
    FileHeader: FileHeader.Header
    OptionalHeader: Optional.Header
} with
    static member Parse (stream:Stream) =
        let signature = consumeChars 4 stream
        
        let fileHeader = FileHeader.Header.Parse stream
        let optionalHeader = Optional.Header.Parse stream
        {
            Signature = signature
            FileHeader = fileHeader
            OptionalHeader = optionalHeader
        }

    member header.Format =
        let builder = new StringBuilder("COFF HEADER\n")

        let fileHeaderReport = header.FileHeader.Format
        let optionalHeaderReport = header.OptionalHeader.Format
        appendCharList builder "Signature" header.Signature
        builder
            .AppendLine(fileHeaderReport)
            .AppendLine(optionalHeaderReport)
            .AppendLine()
            .ToString()