﻿module BanksySan.Tooling.PeDisassembler.Coff.FileHeader

open System
open System.IO
open System.Text
open BanksySan.Tooling.PeDisassembler.Formatting

type Signature = Signature of Value<char list>

[<Flags>]
type Characteristics = 
/// <summary>
///     Image only, Windows CE, and Microsoft Windows NT and later. This indicates that the file does not contain base
///     relocations and must therefore be loaded at its preferred base address. If the base address is not available, the
///     loader reports an error. The default behavior of the linker is to strip base relocations from executable (EXE)
///     files.
/// </summary>
| ImageFileRelocationsStripped = 0x0001

/// <summary>
///     Image only. This indicates that the image file is valid and can be run. If this flag is not set, it indicates a
///     linker error.
/// </summary>
| ImageFileExecutableImage = 0x0002

/// <summary>
///     COFF line numbers have been removed. This flag is deprecated and should be zero.
/// </summary>
| ImageFileLineNumbersStripped = 0x0004

/// <summary>
///     COFF symbol table entries for local symbols have been removed. This flag is deprecated and should be zero.
/// </summary>
| ImageFileLocalSymbolsStripped = 0x0008

/// <summary>
///     Obsolete. Aggressively trim working set. This flag is deprecated for Windows 2000 and later and must be zero.
/// </summary>
| ImageFileAggressiveWsTrim = 0x0010

/// <summary>
///     Application can handle > 2-GB addresses.
/// </summary>
| ImageFileLargeAddressAware = 0x0020

/// <summary>
///     This flag is reserved for future use.
/// </summary>
| ReservedForFutureUse = 0x0040

/// <summary>
///     Little endian: the least significant bit (LSB) precedes the most significant bit (MSB) in memory. This flag is
///     deprecated and should be zero.
/// </summary>
| ImageFileBytesReversedLo = 0x0080

/// <summary>
///     Machine is based on a 32-bit-word architecture.
/// </summary>
| ImageFile32BitMachine = 0x0100

/// <summary>
///     Debugging information is removed from the image file.
/// </summary>
| ImageFileDebugStripped = 0x0200

/// <summary>
///     If the image is on removable media, fully load it and copy it to the swap file.
/// </summary>
| ImageFileRemovableRunFromSwap = 0x0400

/// <summary>
///     If the image is on network media, fully load it and copy it to the swap file.
/// </summary>
| ImageFileNetRunFromSwap = 0x0800

/// <summary>
///     The image file is a system file, not a user program.
/// </summary>
| ImageFileSystem = 0x1000

/// <summary>
///     The image file is a dynamic-link library (DLL). Such files are considered executable files for almost all purposes,
///     although they cannot be directly run.
/// </summary>
| ImageFileDll = 0x2000

/// <summary>
///     The file should be run only on a uniprocessor machine.
/// </summary>
| ImageFileUpSystemOnly = 0x4000

/// <summary>
///     Big endian: the MSB precedes the LSB in memory. This flag is deprecated and should be zero.
/// </summary>
| ImageFileBytesReversedHi = 0x8000

type Machines = 
/// <summary>
///     The contents of this field are assumed to be applicable to any machine type
/// </summary>
| ImageFileMachineUnknown =  0x0us

/// <summary>
///     Matsushita AM33
/// </summary>
| ImageFileMachineAm33 = 0x1d3us

/// <summary>
///     x64
/// </summary>
| ImageFileMachineAmd64 = 0x8664us

/// <summary>
///     ARM little endian
/// </summary>
| ImageFileMachineArm = 0x1c0us

/// <summary>
///     ARM64 little endian
/// </summary>
| ImageFileMachineArm64 = 0xaa64us

/// <summary>
///     ARM Thumb-2 little endian
/// </summary>
| ImageFileMachineArmNt = 0x1c4us

/// <summary>
///     EFI byte code
/// </summary>
| ImageFileMachineEbc = 0xebcus

/// <summary>
///     Intel 386 or later processors and compatible processors
/// </summary>
| ImageFileMachineI386 = 0x14cus

/// <summary>
///     Intel Itanium processor family
/// </summary>
| ImageFileMachineIa64 = 0x200us

/// <summary>
///     Mitsubishi M32R little endian
/// </summary>
| ImageFileMachineM32R = 0x9041us

/// <summary>
///     MIPS16
/// </summary>
| ImageFileMachineMips16 = 0x266us

/// <summary>
///     MIPS with FPU
/// </summary>
| ImageFileMachineMipsFpu = 0x366us

/// <summary>
///     MIPS16 with FPU
/// </summary>
| ImageFileMachineMipsFpu16 = 0x466us

/// <summary>
///     Power PC little endian
/// </summary>
| ImageFileMachinePowerPc = 0x1f0us

/// <summary>
///     Power PC with floating point support
/// </summary>
| ImageFileMachinePowerPcFp = 0x1f1us

/// <summary>
///     MIPS little endian
/// </summary>
| ImageFileMachineR4000 = 0x166us

/// <summary>
///     RISC-V 32-bit address space
/// </summary>
| ImageFileMachineRiscV32 = 0x5032us

/// <summary>
///     RISC-V 64-bit address space
/// </summary>
| ImageFileMachineRiscV64 = 0x5064us

/// <summary>
///     RISC-V 128-bit address space
/// </summary>
| ImageFileMachineRiscv128 = 0x5128us

/// <summary>
///     Hitachi SH3
/// </summary>
| ImageFileMachineSh3 = 0x1a2us

/// <summary>
///     Hitachi SH3 DSP
/// </summary>
| ImageFileMachineSh3Dsp = 0x1a3us

/// <summary>
///     Hitachi SH4
/// </summary>
| ImageFileMachineSh4 = 0x1a6us

/// <summary>
///     Hitachi SH5
/// </summary>
| ImageFileMachineSh5 = 0x1a8us

/// <summary>
///     Thumb
/// </summary>
| ImageFileMachineThumb = 0x1c2us

/// <summary>
///     MIPS little-endian WCE v2
/// </summary>
| ImageFileMachineWceMipsV2 = 0x169us

type Header = {
    Machine: Value<Machines>
    NumberOfSections: Value<uint16>
    TimeDateStamp: Value<DateTime>
    PointerToSymbolTable: Value<uint32>
    NumberOfSymbols: Value<uint32>
    SizeOfOptionalHeader: Value<uint16>
    Characteristics: Value<Characteristics>
} with
static member Parse (stream:Stream) =
    let machineRaw = consumeUInt16 stream
    let machineType = Enum.ToObject(typeof<Machines>, machineRaw.Value) :?> Machines
    let machine = {Offset = machineRaw.Offset; Value = machineType}
    let numberOfSections = consumeUInt16 stream
    let timeDateStamp = consumeUInt32 stream |> convertToDateTime
    let pointerToSymbolTable = consumeUInt32 stream
    let numberOfSymbols = consumeUInt32 stream
    let sizeOfOptionalHeader = consumeUInt16 stream
    let characteristicsRaw = consumeUInt16 stream
    let characteristics = {Offset = characteristicsRaw.Offset; Value = Enum.ToObject(typeof<Characteristics>, characteristicsRaw.Value) :?> Characteristics}

    {
        Machine = machine
        NumberOfSections = numberOfSections
        TimeDateStamp = timeDateStamp
        PointerToSymbolTable = pointerToSymbolTable
        NumberOfSymbols = numberOfSymbols
        SizeOfOptionalHeader = sizeOfOptionalHeader
        Characteristics = characteristics
    }

member header.Format  =

    let appendMachine builder (value:Value<Machines>) =
        let offset = value.Offset
        let value = value.Value
        Printf.bprintf builder "0x%04X %-35s %30A\n" offset "Machine" value

    let appendCharacteristics builder (value:Value<Characteristics>) =
        let offset = value.Offset
        let value = value.Value
        Printf.bprintf builder "0x%04X Characteristics\n" offset 
        for flag in Enum.GetValues(typeof<Characteristics>) :?> Characteristics array do
            let flagString = flag.ToString()
            let hasFlag = value.HasFlag(flag)
            Printf.bprintf builder "    |%-40s%-5b\n" flagString hasFlag

    let builder = new StringBuilder("COFF FILE HEADER\n");
    appendMachine builder header.Machine
    appendUInt16 builder "NumberOfSections" header.NumberOfSections
    appendTimeDateStamp builder "TimeDateStamp" header.TimeDateStamp
    appendUInt32 builder "PointerToSymbolTable" header.PointerToSymbolTable
    appendUInt32 builder "NumberOfSymbols" header.NumberOfSymbols
    appendUInt16 builder "SizeOfOptionslHeader" header.SizeOfOptionalHeader
    appendCharacteristics builder header.Characteristics
    builder
        .AppendLine()
        .ToString()