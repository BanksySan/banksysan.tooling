﻿module BanksySan.Tooling.PeDisassembler.Coff.Optional

open System
open System.IO
open System.Text
open BanksySan.Tooling.PeDisassembler.Formatting
open System.Security.Authentication.ExtendedProtection

type Magic =
| I386MAGIC = 0x0014cus
| C6000  = 0x0108us
| PE32 = 0x010Bus
| PE32PLUS = 0x020Bus

type WindowsSubsystems =
/// <summary> An unknown subsystem</summary>
| ImageSubsystemUnknown = 0x00uy
/// <summary> Device drivers and native Windows processes</summary>
| ImageSubsystemNative = 0x01uy
/// <summary> The Windows graphical user interface (GUI) subsystem</summary>
| ImageSubsystemWindowsGui = 0x02uy
/// <summary> The Windows character subsystem</summary>
| ImageSubsystemWindowsCui = 0x03uy
/// <summary> The OS/2 character subsystem</summary>
| ImageSubsystemOs2Cui = 0x05uy
/// <summary> The Posix character subsystem</summary>
| ImageSubsystemPosixCui = 0x07uy
/// <summary> Native Win9x driver</summary>
| ImageSubsystemNativeWindows = 0x08uy
/// <summary> Windows CE</summary>
| ImageSubsystemWindowsCeGui = 0x09uy
/// <summary> An Extensible Firmware Interface (EFI) application</summary>
| ImageSubsystemEfiApplication = 0x0Auy
/// <summary> An EFI driver with boot services</summary>
| ImageSubsystemEfiBootServiceDriver = 0x0Buy
/// <summary> An EFI driver with run-time services</summary>
| ImageSubsystemEfiRuntimeDriver = 0x0Cuy
/// <summary> An EFI ROM image</summary>
| ImageSubsystemEfiRom = 0x0Duy
/// <summary> XBOX</summary>
| ImageSubsystemXbox = 0x0Euy
/// <summary> Windows boot application.</summary>
| ImageSubsystemWindowsBootApplication = 0x1uy

[<Flags>] 
type DllCharacteristics =
/// <summary>Reserved, must be zero.</summary>
| Reserved1 = 0x0001s
/// <summary>Reserved, must be zero.</summary>
| Reserved2 = 0x0002s
/// <summary>Reserved, must be zero.</summary>
| Reserved3 = 0x0004s
/// <summary>Reserved, must be zero.</summary>
| Reserved4 = 0x0008s
/// <summary>Image can handle a high entropy 64-bit virtual address space.</summary>
| ImageDllCharacteristicsHighEntropyVa = 0x0020s
/// <summary>DLL can be relocated at load time.</summary>
| ImageDllCharacteristicsDynamicBase = 0x0040s
/// <summary>Code Integrity checks are enforced.</summary>
| ImageDllCharacteristicsForceIntegrity = 0x0080s
/// <summary>Image is NX compatible.</summary>
| ImageDllCharacteristicsNxCompat = 0x0100s
/// <summary>Isolation aware, but do not isolate the image.</summary>
| ImageDllCharacteristicsNoIsolation = 0x0200s
/// <summary>Does not use structured exception (SE) handling. No SE handler may be called in this image.</summary>
| ImageDllCharacteristicsNoSeh = 0x0400s
/// <summary>Do not bind the image.</summary>
| ImageDllCharacteristicsNoBind = 0x0800s
/// <summary>Image must execute in an AppContainer.</summary>
| ImageDllCharacteristicsAppContainer = 0x1000s
/// <summary>A WDM driver.</summary>
| ImageDllCharacteristicsWdmDriver = 0x2000s
/// <summary>Image supports Control Flow Guard.</summary>
| ImageDllCharacteristicsGuardCf = 0x4000s
/// <summary>Terminal Server aware.</summary>
| ImageDllCharacteristicsTerminalServerAware = 0x8000s

type Pe32StandardFields = {
    /// <summary>The unsigned integer that identifies the state of the image file. The most common number is 0x10B, which identifies it as a normal executable file. 0x107 identifies it as a ROM image, and 0x20B identifies it as a PE32+ executable.</summary>
    Magic: Value<Magic>
    /// <summary>The linker major version number.</summary>
    MajorLinkerVersion: Value<byte>
    /// <summary>The linker minor version number.</summary>
    MinorLinkerVersion: Value<byte>
    /// <summary>The size of the code (text) section, or the sum of all code sections if there are multiple sections.</summary>
    SizeOfCode: Value<uint32>
    /// <summary>The size of the initialized data section, or the sum of all such sections if there are multiple data sections.</summary>
    SizeOfInitialisedData: Value<uint32>
    /// <summary>The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections.</summary>
    SizeOfUninitialisedData: Value<uint32>
    /// <summary>The address of the entry point relative to the image base when the executable file is loaded into memory. For program images, this is the starting address. For device drivers, this is the address of the initialization function. An entry point is optional for DLLs. When no entry point is present, this field must be zero.</summary>
    AddressOfEntryPoint: Value<uint32>
    /// <summary>The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.</summary>
    BaseOfCode: Value<uint32>
    /// <summary>The address that is relative to the image base of the beginning-of-data section when it is loaded into memory.</summary>
    BaseOfData:Value<uint32>
} with 
    static member Parse magic (stream:Stream) =
        let majorLinkerVersion = consumeByte stream
        let minorLinkerVersion = consumeByte stream
        let sizeOfCode = consumeUInt32 stream
        let sizeOfInitialisedData = consumeUInt32 stream
        let sizeOfUninitialisedData = consumeUInt32 stream
        let addressOfEntryPoint = consumeUInt32 stream
        let baseOfCode = consumeUInt32 stream
        let baseOfData = consumeUInt32 stream
        {
            Magic = magic
            MajorLinkerVersion = majorLinkerVersion
            MinorLinkerVersion = minorLinkerVersion
            SizeOfCode = sizeOfCode
            SizeOfInitialisedData = sizeOfInitialisedData
            SizeOfUninitialisedData = sizeOfUninitialisedData
            AddressOfEntryPoint = addressOfEntryPoint
            BaseOfCode = baseOfCode
            BaseOfData = baseOfData
        }

    member header.Format =
        let appendMagic builder (value:Value<Magic>) =
            let offset = value.Offset
            let value = value.Value
            Printf.bprintf builder "0x%04X %-35s %30A\n" offset "Magic" value

        let builder = new StringBuilder("STANDARD FIELDS")
        appendMagic builder header.Magic
        appendByte builder "MajorLinkerVersion" header.MajorLinkerVersion
        appendByte builder "MinorLinkerVersion" header.MinorLinkerVersion
        appendUInt32 builder "SizeOfCode" header.SizeOfCode
        appendUInt32 builder "SizeOfInitialisedData" header.SizeOfInitialisedData
        appendUInt32 builder "SizeOfUninitialisedData" header.SizeOfUninitialisedData
        appendUInt32 builder "AddressOfEntryPoint" header.AddressOfEntryPoint
        appendUInt32 builder "BaseOfCode" header.BaseOfCode
        appendUInt32 builder "BaseOfData" header.BaseOfData

        builder
            .AppendLine()
            .ToString()

type Pe32PlusStandardFields = {
    /// <summary>The unsigned integer that identifies the state of the image file. The most common number is 0x10B, which identifies it as a normal executable file. 0x107 identifies it as a ROM image, and 0x20B identifies it as a PE32+ executable. </summary>
    Magic: Value<Magic>
    /// <summary>The linker major version number.</summary>
    MajorLinkerVersion: Value<byte>
    /// <summary>The linker minor version number.</summary>
    MinorLinkerVersion: Value<byte>
    /// <summary>The size of the code (text) section, or the sum of all code sections if there are multiple sections.</summary>
    SizeOfCode: Value<uint32>
    /// <summary>The size of the initialized data section, or the sum of all such sections if there are multiple data sections.</summary>
    SizeOfInitialisedData: Value<uint32>
    /// <summary>The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections.</summary>
    SizeOfUninitialisedData: Value<uint32>
    /// <summary>The address of the entry point relative to the image base when the executable file is loaded into memory. For program images, this is the starting address. For device drivers, this is the address of the initialization function. An entry point is optional for DLLs. When no entry point is present, this field must be zero. </summary>
    AddressOfEntryPoint: Value<uint32>
    /// <summary> The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.</summary>
    BaseOfCode: Value<uint32>
} with
    static member Parse magic (stream:Stream) =
        let majorLinkerVersion = consumeByte stream
        let minorLinkerVersion = consumeByte stream
        let sizeOfCode = consumeUInt32 stream
        let sizeOfInitialisedData = consumeUInt32 stream
        let sizeOfUninitialisedData = consumeUInt32 stream
        let addressOfEntryPoint = consumeUInt32 stream
        let baseOfCode = consumeUInt32 stream

        {
            Magic = magic
            MajorLinkerVersion = majorLinkerVersion
            MinorLinkerVersion = minorLinkerVersion
            SizeOfCode = sizeOfCode
            SizeOfInitialisedData = sizeOfInitialisedData
            SizeOfUninitialisedData = sizeOfUninitialisedData
            AddressOfEntryPoint = addressOfEntryPoint
            BaseOfCode = baseOfCode
        }

    member header.Format =
        let appendMagic builder (value:Value<Magic>) =
            let offset = value.Offset
            let value = value.Value
            Printf.bprintf builder "0x%04X %-35s %30A\n" offset "Magic" value

        let builder = new StringBuilder("STANDARD FIELDS")
        appendMagic builder header.Magic
        appendByte builder "MajorLinkerVersion" header.MajorLinkerVersion
        appendByte builder "MinorLinkerVersion" header.MinorLinkerVersion
        appendUInt32 builder "SizeOfCode" header.SizeOfCode
        appendUInt32 builder "SizeOfInitialisedData" header.SizeOfInitialisedData
        appendUInt32 builder "SizeOfUninitialisedData" header.SizeOfUninitialisedData
        appendUInt32 builder "AddressOfEntryPoint" header.AddressOfEntryPoint
        appendUInt32 builder "BaseOfCode" header.BaseOfCode

        builder
            .AppendLine()
            .ToString()

type StandardFields =
| Pe32 of Pe32StandardFields
| Pe32Plus of Pe32PlusStandardFields
    static member Parse (stream:Stream) =

        let magicRaw = consumeUInt16 stream
        let magic = {
            Offset = magicRaw.Offset
            Value = Enum.ToObject(typeof<Magic>, magicRaw.Value) :?> Magic
        }

        match magic.Value with
        | Magic.PE32 -> Pe32StandardFields.Parse magic stream |> Pe32
        | Magic.PE32PLUS -> Pe32PlusStandardFields.Parse magic stream |> Pe32Plus
        | _ -> failwithf "Unable to parse optional header with magic '%O'" magic
    member header.Format =
            match header with
            | Pe32 x -> x.Format
            | Pe32Plus x -> x.Format

type Pe32WindowsSpecificFields = {
    /// <summary>The preferred address of the first byte of image when loaded into memory; must be a multiple of 64 K. The defaultfor DLLs is 0x10000000. The default for Windows CE EXEs is 0x00010000. The default for Windows NT, Windows 2000,Windows XP, Windows 95, Windows 98, and Windows Me is 0x00400000.</summary>
    ImageBase:Value<uint32>
    /// <summary>The alignment (in bytes) of sections when they are loaded into memory. It must be greater than or equal toFileAlignment. The default is the page size for the architecture.</summary>
    SectionAlignment:Value<uint32>
    /// <summary>The alignment factor (in bytes) that is used to align the raw data of sections in the image file. The value shouldbe a power of 2 between 512 and 64 K, inclusive. The default is 512. If the SectionAlignment is less than thearchitecture's page size, then FileAlignment must match SectionAlignment.</summary>
    FileAlignment:Value<uint32>
    /// <summary>The major version number of the required operating system.</summary>
    MajorOperatingSystemVersion:Value<uint16>
    /// <summary>The minor version number of the required operating system.</summary>
    MinorOperatingSystemVersion:Value<uint16>
    /// <summary>The major version number of the image.</summary>
    MajorImageVersion:Value<uint16>
    /// <summary>The minor version number of the image.</summary>
    MinorImageVersion:Value<uint16>
    /// <summary>The major version number of the subsystem.</summary>
    MajorSubsystemVersion:Value<uint16>
    /// <summary>The minor version number of the subsystem.</summary>
    MinorSubsystemVersion:Value<uint16>
    /// <summary>Reserved, must be zero.</summary>
    Win32VersionValue:Value<uint32>
    /// <summary>The size (in bytes) of the image, including all headers, as the image is loaded in memory. It must be a multiple ofSectionAlignment.</summary>
    SizeOfImage:Value<uint32>
    /// <summary>The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment.</summary>
    SizeOfHeaders:Value<uint32>
    /// <summary>The image file checksum. The algorithm for computing the checksum is incorporated into IMAGHELP.DLL. The followingare checked for validation at load time: all drivers, any DLL loaded at boot time, and any DLL that is loaded intoa critical Windows process.</summary>
    CheckSum:Value<uint32>
    /// <summary>The subsystem that is required to run this image.</summary>
    Subsystem:Value<WindowsSubsystems>
    /// <summary>DLL Characteristics.</summary>
    DllCharacteristics:Value<DllCharacteristics>
    /// <summary>The size of the stack to reserve. Only SizeOfStackCommit is committed; the rest is made available one page at atime until the reserve size is reached.</summary>
    SizeOfStackReserve:Value<uint32>
    /// <summary>The size of the stack to commit.</summary>
    SizeOfStackCommit:Value<uint32>
    /// <summary>The size of the local heap space to reserve. Only SizeOfHeapCommit is committed; the rest is made available onepage at a time until the reserve size is reached.</summary>
    SizeOfHeapReserve:Value<uint32>
    /// <summary>The size of the local heap space to commit.</summary>
    SizeOfHeapCommit:Value<uint32>
    /// <summary>Reserved, must be zero.</summary>
    LoaderFlags:Value<uint32>
    /// <summary>The number of data-directory entries in the remainder of the optional header. Each describes a location and size.</summary>
    NumberOfRvaAndSizes:Value<uint32>
} with
    static member Parse stream =
        let consumeWindowsSubsytems stream =
            let raw = consumeUInt16 stream
            let num = Enum.ToObject(typeof<WindowsSubsystems>, raw.Value) :?> WindowsSubsystems
            {Offset = raw.Offset; Value = num}

        let consumeDllCharacteristics stream =
            let raw = consumeUInt16 stream
            let num = Enum.ToObject(typeof<DllCharacteristics>, raw.Value) :?> DllCharacteristics
            {Offset = raw.Offset; Value = num}

        let imageBase = consumeUInt32 stream
        let sectionAlignment = consumeUInt32 stream
        let fileAlignment = consumeUInt32 stream
        let majorOperatingSystemVersion = consumeUInt16 stream
        let minorOperatingSystemVersion = consumeUInt16 stream
        let majorImageVersion = consumeUInt16 stream
        let minorImageVersion = consumeUInt16 stream
        let majorSubsystemVersion = consumeUInt16 stream
        let minorSubsystemVersion = consumeUInt16 stream
        let win32VersionValue = consumeUInt32 stream
        let sizeOfImage = consumeUInt32 stream
        let sizeOfHeaders = consumeUInt32 stream
        let checkSum = consumeUInt32 stream
        let subsystem = consumeWindowsSubsytems stream
        let dllCharacteristics = consumeDllCharacteristics stream
        let sizeOfStackReserve = consumeUInt32 stream
        let sizeOfStackCommit = consumeUInt32 stream
        let sizeOfHeapReserve = consumeUInt32 stream
        let sizeOfHeapCommit = consumeUInt32 stream
        let loaderFlags = consumeUInt32  stream
        let numberOfRvaAndSizes = consumeUInt32  stream

        {
            ImageBase = imageBase
            SectionAlignment = sectionAlignment
            FileAlignment = fileAlignment
            MajorOperatingSystemVersion = majorOperatingSystemVersion
            MinorOperatingSystemVersion = minorOperatingSystemVersion
            MajorImageVersion = majorImageVersion
            MinorImageVersion = minorImageVersion
            MajorSubsystemVersion = majorSubsystemVersion
            MinorSubsystemVersion = minorSubsystemVersion
            Win32VersionValue = win32VersionValue
            SizeOfImage = sizeOfImage
            SizeOfHeaders = sizeOfHeaders
            CheckSum = checkSum
            Subsystem = subsystem
            DllCharacteristics = dllCharacteristics
            SizeOfStackReserve = sizeOfStackReserve
            SizeOfStackCommit = sizeOfStackCommit
            SizeOfHeapReserve = sizeOfHeapReserve
            SizeOfHeapCommit = sizeOfHeapCommit
            LoaderFlags = loaderFlags
            NumberOfRvaAndSizes = numberOfRvaAndSizes
        }
    member this.Format =
        let appendSubsystem builder (value:Value<WindowsSubsystems>) =
            let offset = value.Offset
            let value = value.Value
            Printf.bprintf builder "0x%04X %-35s %30A\n" offset "WindowsSubsystems" value
        
        let appendDllCharacteristics builder (value:Value<DllCharacteristics>) =
            let offset = value.Offset
            let value = value.Value
            Printf.bprintf builder "0x%04X DllCharacteristics\n" offset 
            for flag in Enum.GetValues(typeof<DllCharacteristics>) :?> DllCharacteristics array do
                let flagString = flag.ToString()
                let hasFlag = value.HasFlag(flag)
                Printf.bprintf builder "    |%-50s%-5b\n" flagString hasFlag
        let builder = new StringBuilder("WINDOWS SPECIFIC (PE32)\n")
        appendUInt32 builder "ImageBase" this.ImageBase
        appendUInt32 builder "SectionAlignment" this.SectionAlignment
        appendUInt32 builder "FileAlignment" this.FileAlignment
        appendUInt16 builder "MajorOperatingSystemVersion" this.MajorOperatingSystemVersion
        appendUInt16 builder "MinorOperatingSystemVersion" this.MinorOperatingSystemVersion
        appendUInt16 builder "MajorImageVersion" this.MajorImageVersion
        appendUInt16 builder "MinorImageVersion" this.MinorImageVersion
        appendUInt16 builder "MajorSubsystemVersion" this.MajorSubsystemVersion
        appendUInt16 builder "MinorSubsystemVersion" this.MinorSubsystemVersion
        appendUInt32 builder "Win32VersionValue" this.Win32VersionValue
        appendUInt32 builder "SizeOfImage" this.SizeOfImage
        appendUInt32 builder "SizeOfHeaders" this.SizeOfHeaders
        appendUInt32 builder "CheckSum" this.CheckSum
        appendSubsystem builder this.Subsystem
        appendDllCharacteristics builder this.DllCharacteristics
        appendUInt32 builder "SizeOfStackReserve" this.SizeOfStackReserve
        appendUInt32 builder "SizeOfStackCommit" this.SizeOfStackCommit
        appendUInt32 builder "SizeOfHeapReserve" this.SizeOfHeapReserve
        appendUInt32 builder "SizeOfHeapCommit" this.SizeOfHeapCommit
        appendUInt32 builder "LoaderFlags" this.LoaderFlags
        appendUInt32 builder "NumberOfRvaAndSizes" this.NumberOfRvaAndSizes
        builder
            .AppendLine()
            .ToString()

type Ps32PlusWindowsSpecificFields = {
    /// <summary> The preferred address of the first byte of image when loaded into memory; must be a multiple of 64 K. The default for DLLs is 0x10000000. The default for Windows CE EXEs is 0x00010000. The default for Windows NT, Windows 2000, Windows XP, Windows 95, Windows 98, and Windows Me is 0x00400000.</summary>
    ImageBase: Value<uint64>
    /// <summary> The alignment (in bytes) of sections when they are loaded into memory. It must be greater than or equal to FileAlignment. The default is the page size for the architecture.</summary>
    SectionAlignment: Value<uint32>
    /// <summary> The alignment factor (in bytes) that is used to align the raw data of sections in the image file. The value should be a power of 2 between 512 and 64 K, inclusive. The default is 512. If the SectionAlignment is less than the architecture's page size, then FileAlignment must match SectionAlignment.</summary>
    FileAlignment: Value<uint32>
    /// <summary> The major version number of the required operating system.</summary>
    MajorOperatingSystemVersion: Value<uint16>
    /// <summary> The minor version number of the required operating system.</summary>
    MinorOperatingSystemVersion: Value<uint16>
    /// <summary> The major version number of the image.</summary>
    MajorImageVersion: Value<uint16>
    /// <summary> The minor version number of the image.</summary>
    MinorImageVersion: Value<uint16>
    /// <summary> The major version number of the subsystem.</summary>
    MajorSubsystemVersion: Value<uint16>
    /// <summary> The minor version number of the subsystem.</summary>
    MinorSubsystemVersion: Value<uint16>
    /// <summary> Reserved, must be zero.</summary>
    Win32VersionValue: Value<uint32>
    /// <summary> The size (in bytes) of the image, including all headers, as the image is loaded in memory. It must be a multiple of SectionAlignment.</summary>
    SizeOfImage: Value<uint32>
    /// <summary> The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment.</summary>
    SizeOfHeaders: Value<uint32>
    /// <summary> The image file checksum. The algorithm for computing the checksum is incorporated into IMAGHELP.DLL. The following are checked for validation at load time: all drivers, any DLL loaded at boot time, and any DLL that is loaded into a critical Windows process.</summary>
    CheckSum: Value<uint32>
    /// <summary> The subsystem that is required to run this image.</summary>
    Subsystem: Value<WindowsSubsystems>
    /// <summary> For more information, see DLL Characteristics later in this specification.</summary>
    DllCharacteristics: Value<DllCharacteristics>
    /// <summary> The size of the stack to reserve. Only SizeOfStackCommit is committed; the rest is made available one page at a time until the reserve size is reached.</summary>
    SizeOfStackReserve: Value<uint64>
    /// <summary> The size of the stack to commit.</summary>
    SizeOfStackCommit: Value<uint64>
    /// <summary> The size of the local heap space to reserve. Only SizeOfHeapCommit is committed; the rest is made available one page at a time until the reserve size is reached.</summary>
    SizeOfHeapReserve: Value<uint64>
    /// <summary> The size of the local heap space to commit.</summary>
    SizeOfHeapCommit: Value<uint64>
    /// <summary> Reserved, must be zero.</summary>
    LoaderFlags: Value<uint32>
    /// <summary> The number of data-directory entries in the remainder of the optional header. Each describes a location and size.</summary>
    NumberOfRvaAndSizes: Value<uint32>
} with
    static member Parse stream =
        let consumeWindowsSubsytems stream =
            let raw = consumeUInt16 stream
            let num = Enum.ToObject(typeof<WindowsSubsystems>, raw.Value) :?> WindowsSubsystems
            {Offset = raw.Offset; Value = num}
        let consumeDllCharacteristics stream =
            let raw = consumeUInt16 stream
            let num = Enum.ToObject(typeof<DllCharacteristics>, raw.Value) :?> DllCharacteristics
            {Offset = raw.Offset; Value = num}

        let imageBase = consumeUInt64 stream
        let sectionAlignment = consumeUInt32 stream
        let fileAlignment = consumeUInt32 stream
        let majorOperatingSystemVersion = consumeUInt16 stream
        let minorOperatingSystemVersion = consumeUInt16 stream
        let majorImageVersion = consumeUInt16 stream
        let minorImageVersion = consumeUInt16 stream
        let majorSubsystemVersion = consumeUInt16 stream
        let minorSubsystemVersion = consumeUInt16 stream
        let win32VersionValue = consumeUInt32 stream
        let sizeOfImage = consumeUInt32 stream
        let sizeOfHeaders = consumeUInt32 stream
        let checkSum = consumeUInt32 stream
        let subsystem = consumeWindowsSubsytems stream
        let dllCharacteristics = consumeDllCharacteristics stream
        let sizeOfStackReserve = consumeUInt64 stream
        let sizeOfStackCommit = consumeUInt64 stream
        let sizeOfHeapReserve = consumeUInt64 stream
        let sizeOfHeapCommit = consumeUInt64 stream
        let loaderFlags = consumeUInt32  stream
        let numberOfRvaAndSizes = consumeUInt32  stream

        {
            ImageBase = imageBase
            SectionAlignment = sectionAlignment
            FileAlignment = fileAlignment
            MajorOperatingSystemVersion = majorOperatingSystemVersion
            MinorOperatingSystemVersion = minorOperatingSystemVersion
            MajorImageVersion = majorImageVersion
            MinorImageVersion = minorImageVersion
            MajorSubsystemVersion = majorSubsystemVersion
            MinorSubsystemVersion = minorSubsystemVersion
            Win32VersionValue = win32VersionValue
            SizeOfImage = sizeOfImage
            SizeOfHeaders = sizeOfHeaders
            CheckSum = checkSum
            Subsystem = subsystem
            DllCharacteristics = dllCharacteristics
            SizeOfStackReserve = sizeOfStackReserve
            SizeOfStackCommit = sizeOfStackCommit
            SizeOfHeapReserve = sizeOfHeapReserve
            SizeOfHeapCommit = sizeOfHeapCommit
            LoaderFlags = loaderFlags
            NumberOfRvaAndSizes = numberOfRvaAndSizes
        }
    member this.Format =
        let appendSubsystem builder (value:Value<WindowsSubsystems>) =
            let offset = value.Offset
            let value = value.Value
            Printf.bprintf builder "0x%04X %-35s %30A\n" offset "WindowsSubsystems" value
        
        let appendDllCharacteristics builder (value:Value<DllCharacteristics>) =
            let offset = value.Offset
            let value = value.Value
            Printf.bprintf builder "0x%04X DllCharacteristics\n" offset 
            for flag in Enum.GetValues(typeof<DllCharacteristics>) :?> DllCharacteristics array do
                let flagString = flag.ToString()
                let hasFlag = value.HasFlag(flag)
                Printf.bprintf builder "    |%-40s%-5b\n" flagString hasFlag
        let builder = new StringBuilder("WINDOWS SPECIFIC (PE32)\n")
        appendUInt64 builder "ImageBase" this.ImageBase
        appendUInt32 builder "SectionAlignment" this.SectionAlignment
        appendUInt32 builder "FileAlignment" this.FileAlignment
        appendUInt16 builder "MajorOperatingSystemVersion" this.MajorOperatingSystemVersion
        appendUInt16 builder "MinorOperatingSystemVersion" this.MinorOperatingSystemVersion
        appendUInt16 builder "MajorImageVersion" this.MajorImageVersion
        appendUInt16 builder "MinorImageVersion" this.MinorImageVersion
        appendUInt16 builder "MajorSubsystemVersion" this.MajorSubsystemVersion
        appendUInt16 builder "MinorSubsystemVersion" this.MinorSubsystemVersion
        appendUInt32 builder "Win32VersionValue" this.Win32VersionValue
        appendUInt32 builder "SizeOfImage" this.SizeOfImage
        appendUInt32 builder "SizeOfHeaders" this.SizeOfHeaders
        appendUInt32 builder "CheckSum" this.CheckSum
        appendSubsystem builder this.Subsystem
        appendDllCharacteristics builder this.DllCharacteristics
        appendUInt64 builder "SizeOfStackReserve" this.SizeOfStackReserve
        appendUInt64 builder "SizeOfStackCommit" this.SizeOfStackCommit
        appendUInt64 builder "SizeOfHeapReserve" this.SizeOfHeapReserve
        appendUInt64 builder "SizeOfHeapCommit" this.SizeOfHeapCommit
        appendUInt32 builder "LoaderFlags" this.LoaderFlags
        appendUInt32 builder "NumberOfRvaAndSizes" this.NumberOfRvaAndSizes
        builder
            .AppendLine()
            .ToString()

type WindowsSpecificFields =
    | Pe32 of Pe32WindowsSpecificFields
    | Pe32Plus of Ps32PlusWindowsSpecificFields
    with
        static member Parse magic stream =
            match magic with
            | Magic.PE32 -> 
                stream |> Pe32WindowsSpecificFields.Parse |> Pe32
            | Magic.PE32PLUS -> 
                stream |> Ps32PlusWindowsSpecificFields.Parse |> Pe32Plus
            | unknownMagic -> failwithf "Cannot parse magic '%A'" unknownMagic
        member this.Format =
            match this with
            | Pe32 x -> x.Format
            | Pe32Plus x -> x.Format

type Header = {
    Standard:  StandardFields
    WindowsSpecific: WindowsSpecificFields
} with 
    static member Parse (stream:Stream) =
        let standardFields = StandardFields.Parse stream
        let magic = match standardFields with
                    | StandardFields.Pe32 x -> x.Magic.Value
                    | StandardFields.Pe32Plus x -> x.Magic.Value
        let windowsSpecific = WindowsSpecificFields.Parse magic stream
        {
            Standard = standardFields
            WindowsSpecific = windowsSpecific
        }

    member header.Format =
        let builder = new StringBuilder("OPTIONAL HEADER\n")
        builder
            .AppendLine(header.Standard.Format)
            .AppendLine(header.WindowsSpecific.Format)
            .AppendLine()
            .ToString()