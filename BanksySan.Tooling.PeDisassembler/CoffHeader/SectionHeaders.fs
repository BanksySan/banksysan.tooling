﻿module BanksySan.Tooling.PeDisassembler.Coff.SectionHeaders

open System
open System.Text
open BanksySan.Tooling.PeDisassembler.Formatting

type DataDirectory = {
    /// <summary>The RVA of this table, relative to the base address of the image.</summary>
    VirtualAddress: Value<uint32>
    /// <summary>Size of table in <see cref="System.Byte">bytes</see>.</summary>
    Size: Value<uint32>
} with
    static member ParseOne stream =
        let rva = consumeUInt32 stream
        let size = consumeUInt32 stream
        { VirtualAddress = rva; Size = size }
    static member Parse numberOfRvaAndSizes stream =
        let mutable buffer = List.empty<DataDirectory>
        let mutable counter = 0u

        while counter < numberOfRvaAndSizes do
            counter <- counter + 1u
            let currentDirectory = DataDirectory.ParseOne stream
            buffer <-currentDirectory:: buffer
        buffer |> List.rev
    member this.Format =
        sprintf "0x%04X VirtualAddress:\t0x%08X\t\tSize:\t0x%08X" this.VirtualAddress.Offset this.VirtualAddress.Value this.Size.Value
    static member FormatAll (dataDirectories:DataDirectory list) =
        let builder = new StringBuilder("DATA DIRECTORIES\n")
        dataDirectories |> List.iter (fun directory -> builder.AppendLine(directory.Format) |> ignore)
        builder
            .AppendLine()
            .ToString()

[<Flags>]
type SectionHeaderCharacteristics =
/// <summary>Reserved for future use.</summary>
| RESERVED_1 = 0x00000000ul
/// <summary>Reserved for future use.</summary>
| RESERVED_2 = 0x00000001ul
/// <summary>Reserved for future use.</summary>
| RESERVED_3 = 0x00000002ul
/// <summary>Reserved for future use.</summary>
| RESERVED_4 = 0x00000004ul
/// <summary>The section should not be padded to the next boundary. This flag is obsolete and is replaced by <see cref="IMAGE_SCN_ALIGN_1BYTESa">IMAGE_SCN_ALIGN_1BYTES</see>. This is valid only for object files.</summary>
| IMAGE_SCN_TYPE_NO_PAD = 0x00000008ul
/// <summary>Reserved for future use.</summary>
| RESERVED_5 = 0x00000010ul
/// <summary>The section contains executable code.</summary>
| IMAGE_SCN_CNT_CODE = 0x00000020ul
/// <summary>The section contains initialized data.</summary>
| IMAGE_SCN_CNT_INITIALIZED_DATA = 0x00000040ul
/// <summary>The section contains uninitialized data.</summary>
| IMAGE_SCN_CNT_UNINITIALIZED_DATA = 0x00000080ul
/// <summary>Reserved for future use.</summary>
| IMAGE_SCN_LNK_OTHER = 0x00000100ul
/// <summary>The section contains comments or other information. The <c>.drectve</c> section has this type. This is valid for object files only.</summary>
| IMAGE_SCN_LNK_INFO = 0x00000200ul
/// <summary>Reserved for future use.</summary>
| RESERVED = 0x00000400ul
/// <summary>The section will not become part of the image. This is valid only for object files.</summary>
| IMAGE_SCN_LNK_REMOVE = 0x00000800ul
/// <summary>The section contains COMDAT data. This is only for object files.</summary>
| IMAGE_SCN_LNK_COMDAT = 0x00001000ul
/// <summary>The section contains data referenced through the global pointer (GP).</summary>
| IMAGE_SCN_GPREL = 0x00008000ul
/// <summary>Reserved for future use.</summary>
| IMAGE_SCN_MEM_PURGEABLE = 0x00020000ul
/// <summary>Reserved for future use.</summary>
| IMAGE_SCN_MEM_16BIT = 0x00020000ul
/// <summary>Reserved for future use.</summary>
| IMAGE_SCN_MEM_LOCKED = 0x00040000ul
/// <summary>Reserved for future use.</summary>
| IMAGE_SCN_MEM_PRELOAD = 0x00080000ul
/// <summary>Align data on a 1-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_1BYTES = 0x00100000ul
/// <summary>Align data on a 2-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_2BYTES = 0x00200000ul
/// <summary>Align data on a 4-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_4BYTES = 0x00300000ul
/// <summary>Align data on an 8-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_8BYTES = 0x00400000ul
/// <summary>Align data on a 16-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_16BYTES = 0x00500000ul
/// <summary>Align data on a 32-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_32BYTES = 0x00600000ul
/// <summary>Align data on a 64-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_64BYTES = 0x00700000ul
/// <summary>Align data on a 128-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_128BYTES = 0x00800000ul
/// <summary>Align data on a 256-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_256BYTES = 0x00900000ul
/// <summary>Align data on a 512-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_512BYTES = 0x00A00000ul
/// <summary>Align data on a 1024-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_1024BYTES = 0x00B00000ul
/// <summary>Align data on a 2048-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_2048BYTES = 0x00C00000ul
/// <summary>Align data on a 4096-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_4096BYTES = 0x00D00000ul
/// <summary>Align data on an 8192-byte boundary. Valid only for object files.</summary>
| IMAGE_SCN_ALIGN_8192BYTES = 0x00E00000ul
/// <summary>The section contains extended relocations.</summary>
| IMAGE_SCN_LNK_NRELOC_OVFL = 0x01000000ul
/// <summary>The section can be discarded as needed.</summary>
| IMAGE_SCN_MEM_DISCARDABLE = 0x02000000ul
/// <summary>The section cannot be cached.</summary>
| IMAGE_SCN_MEM_NOT_CACHED = 0x04000000ul
/// <summary>The section is not pageable.</summary>
| IMAGE_SCN_MEM_NOT_PAGED = 0x08000000ul
/// <summary>The section can be shared in memory.</summary>
| IMAGE_SCN_MEM_SHARED = 0x10000000ul
/// <summary>The section can be executed as code.</summary>
| IMAGE_SCN_MEM_EXECUTE = 0x20000000ul
/// <summary>The section can be read.</summary>
| IMAGE_SCN_MEM_READ = 0x40000000ul
/// <summary>The section can be written to.</summary>
| IMAGE_SCN_MEM_WRITE = 0x80000000ul

type SectionHeader = {
    /// <summary>An 8-byte, null-padded UTF-8 encoded string. If the string is exactly 8 characters long, there is no terminating null. For longer names, this field contains a slash (/) that is followed by an ASCII representation of a decimal number that is an offset into the string table. Executable images do not use a string table and do not support section names longer than 8 characters. Long names in object files are truncated if they are emitted to an executable file.</summary>
    Name: Value<char list>
    ///<summary>The total size of the section when loaded into memory. If this value is greater than SizeOfRawData, the section is zero-padded. This field is valid only for executable images and should be set to zero for object files.</summary>
    VirtualSize: Value<uint32>
    /// <summary>For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory. For object files, this field is the address of the first byte before relocation is applied; for simplicity, compilers should set this to zero. Otherwise, it is an arbitrary value that is subtracted from offsets during relocation.</summary>
    VirtualAddress: Value<uint32>
    /// <summary>The size of the section (for object files) or the size of the initialized data on disk (for image files). For executable images, this must be a multiple of FileAlignment from the optional header. If this is less than VirtualSize, the remainder of the section is zero-filled. Because the SizeOfRawData field is rounded but the VirtualSize field is not, it is possible for SizeOfRawData to be greater than VirtualSize as well. When a section contains only uninitialized data, this field should be zero.</summary>
    SizeOfRawData: Value<uint32>
    /// <summary>The file pointer to the first page of the section within the COFF file. For executable images, this must be a multiple of FileAlignment from the optional header. For object files, the value should be aligned on a 4-byte boundary for best performance. When a section contains only uninitialized data, this field should be zero.</summary>
    PointerToRawData: Value<uint32>
    /// <summary>The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.</summary>
    PointerToRelocations: Value<uint32>
    /// <summary>The file pointer to the beginning of line-number entries for the section. This is set to zero if there are no COFF line numbers. This value should be zero for an image because COFF debugging information is deprecated.</summary>
    PointerToLinenumbers: Value<uint32>
    /// <summary>The number of relocation entries for the section. This is set to zero for executable images.</summary>
    NumberOfRelocations: Value<uint16>
    /// <summary>The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated.</summary>
    NumberOfLinenumbers: Value<uint16>
    /// <summary>The flags that describe the characteristics of the section. For more information, see Section Flags</summary>
    Characteristics: Value<SectionHeaderCharacteristics>
} with
    static member Parse stream =
        let consumeCharacteristics stream =
            let raw = consumeUInt32 stream
            let value = Enum.ToObject(typeof<SectionHeaderCharacteristics>, raw.Value) :?> SectionHeaderCharacteristics
            { Value = value; Offset = raw.Offset }

        let name = consumeChars 8 stream
        let virtualSize = consumeUInt32 stream
        let virtualAddress = consumeUInt32 stream
        let sizeOfRawData = consumeUInt32 stream
        let pointerToRawData = consumeUInt32 stream
        let pointerToRelocations = consumeUInt32 stream
        let pointerToLineNumbers = consumeUInt32 stream
        let numberOfRelocations = consumeUInt16 stream
        let numberOfLineNumbers = consumeUInt16 stream
        let characteristics = consumeCharacteristics stream
        {
            Name = name
            VirtualSize = virtualSize
            VirtualAddress = virtualAddress
            SizeOfRawData = sizeOfRawData
            PointerToRawData = pointerToRawData
            PointerToRelocations = pointerToRelocations
            PointerToLinenumbers = pointerToLineNumbers
            NumberOfRelocations = numberOfRelocations
            NumberOfLinenumbers = numberOfLineNumbers
            Characteristics = characteristics
        }
    member this.Format =
        let appendCharacteristics builder (value:Value<SectionHeaderCharacteristics>) =
            let offset = value.Offset
            let value = value.Value
            Printf.bprintf builder "0x%04X SectionHeaderCharacteristics\n" offset 
            for flag in Enum.GetValues(typeof<SectionHeaderCharacteristics>) :?> SectionHeaderCharacteristics array do
                let flagString = flag.ToString()
                let hasFlag = value.HasFlag(flag)
                Printf.bprintf builder "    |%-40s%-5b\n" flagString hasFlag

        let builder = new StringBuilder("SECTION HEADER\n")

        appendCharList builder "Name" this.Name
        appendUInt32 builder "VirtualSize" this.VirtualSize
        appendUInt32 builder "VirtualAddress" this.VirtualAddress
        appendUInt32 builder "SizeOfRawData" this.SizeOfRawData
        appendUInt32 builder "PointerToRawData" this.PointerToRawData
        appendUInt32 builder "PointerToRelocations" this.PointerToRelocations
        appendUInt32 builder "PointerToLineNumbers" this.PointerToLinenumbers
        appendUInt16 builder "NumberOfRelocations" this.NumberOfRelocations
        appendUInt16 builder "NumberOfLineNumbers" this.NumberOfLinenumbers
        appendCharacteristics builder this.Characteristics
        builder 
            .AppendLine()
            .ToString()

type SectionHeaders = {
    DataDirectories: DataDirectory list
    SectionHeaders: SectionHeader list
} with
    static member Parse numberOfSections numberOfRva stream =
        let dataDirectories = DataDirectory.Parse numberOfRva stream
        let mutable sectionCounter = 0us
        let mutable sections = List.empty
        while sectionCounter < numberOfSections do 
            sectionCounter <- sectionCounter + 1us
            let currentSection = SectionHeader.Parse stream
            sections <- currentSection::sections
        sections <- sections |> List.rev
        { DataDirectories = dataDirectories; SectionHeaders = sections}
    member this.Format =
        let builder = new StringBuilder("SECTION HEADERS\n")
        this.DataDirectories |> List.iter (fun dataDirectory -> builder.AppendLine(dataDirectory.Format) |> ignore)
        this.SectionHeaders |> List.iter (fun section-> builder.AppendLine(section.Format) |> ignore)

        builder
            .AppendLine()
            .ToString()
