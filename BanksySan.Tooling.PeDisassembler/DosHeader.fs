﻿module BanksySan.Tooling.PeDisassembler.Dos

open System.IO
open System.Text
open BanksySan.Tooling.PeDisassembler.Formatting

[<Struct>]
type DosHeader = {
    /// <summary>e_magic: 4D 5A (Magic number 'MZ')</summary>
    Signature: Value<char list>
    /// <summary>e_cblp: 0x0090 (Bytes on last page of file)</summary>
    LastPageSize: Value<uint16>
    /// <summary>e_cp: 0x0003 (Pages in file)</summary>
    NumberOfPagesInFile: Value<uint16>
    /// <summary>e_crlc: 0x0000 (Relocations)</summary>
    Relocations: Value<uint16>
    /// <summary>e_cparhdr: 0x0004 (Size of header in paragraphs)</summary>
    HeaderSize: Value<uint16>
    /// <summary>e_minalloc: 0x0000 (Minimum extra paragraphs needed)</summary>
    MinimumAllocation: Value<uint16>
    /// <summary>e_maxalloc: 0xFFFF (Maximum extra paragraphs needed)</summary>
    MaximumAllocation: Value<uint16>
    /// <summary>e_ss: 0x0000 (Initial relative SS value)</summary>
    InitialSsValue: Value<uint16>
    /// <summary>e_sp: 0x00B8 (Initial SP value)</summary>
    InitialSpValue: Value<uint16>
    /// <summary>e_csum: 0x0000  Checksum)</summary>
    Checksum: Value<uint16>
    /// <summary>e_ip: 0x0000 (Initial IP value)</summary>
    InitialIpValue: Value<uint16>
    /// <summary>e_cs: 0x0000 (Initial relative CS value)</summary>
    InitialCsValue: Value<uint16>
    /// <summary>e_lfarlc: 0x0040 (File address of relocation table)</summary>
    FileAddressOfRelocationTable: Value<uint16>
    /// <summary>e_ovno: 0x0000 (Overlay number)</summary>
    OverlayNumber: Value<uint16>
    Reserved1: Value<uint16> list
    /// <summary>e_oemid: 0x0000 (OEM identifier for e_oeminfo)</summary>
    OemIdentifier: Value<uint16>
    /// <summary>e_oeminfo: 0x0000 (OEM information; e_oemid specific)</summary>
    OemInfo: Value<uint16>
    Reserved2: Value<uint16> list
    /// <summary>e_lfanew: 0x00000080 (File address of the new exe header)</summary>
    FileAddressOfNewExeHeader: Value<uint32>
} with
static member Parse (stream:Stream) =
    let signiture = consumeChars 2 stream
    let lastPageSize = consumeUInt16 stream
    let numberOfPagesInFile = consumeUInt16 stream
    let relocations = consumeUInt16 stream
    let headerSize = consumeUInt16 stream
    let minimumAllocation = consumeUInt16 stream
    let maximumAllocation = consumeUInt16 stream
    let initialSs = consumeUInt16 stream
    let initialSp = consumeUInt16 stream
    let checksum = consumeUInt16 stream
    let initialIp = consumeUInt16 stream
    let initialCs = consumeUInt16 stream
    let fileAddressOfRelocationTable = consumeUInt16 stream
    let overlayNumber = consumeUInt16 stream
    let reserverd1 = [for _ in 0..3 ->  consumeUInt16 stream]
    let oemIdentifier = consumeUInt16 stream
    let oemInfo = consumeUInt16 stream
    let reserverd2 = [for _ in 0..9 ->  consumeUInt16 stream]
    let fileAddresOfNewExecutableHeader = consumeUInt32 stream

    { 
        Signature = signiture
        LastPageSize = lastPageSize
        NumberOfPagesInFile = numberOfPagesInFile
        Relocations = relocations
        HeaderSize = headerSize
        MinimumAllocation = minimumAllocation
        MaximumAllocation = maximumAllocation
        InitialSsValue = initialSs
        InitialSpValue = initialSp
        Checksum = checksum
        InitialIpValue = initialIp
        InitialCsValue = initialCs
        FileAddressOfRelocationTable = fileAddressOfRelocationTable
        OverlayNumber = overlayNumber
        Reserved1 = reserverd1
        OemIdentifier = oemIdentifier
        OemInfo = oemInfo
        Reserved2 = reserverd2
        FileAddressOfNewExeHeader = fileAddresOfNewExecutableHeader
    }

member header.Format =
    let builder = new StringBuilder("DOS HEADER\n")

    appendCharList builder "Signature" header.Signature
    appendUInt16 builder "LastPageSize" header.LastPageSize
    appendUInt16 builder "NumberOfPagesInFile" header.NumberOfPagesInFile
    appendUInt16 builder "Relocations" header.Relocations
    appendUInt16 builder "HeaderSize" header.HeaderSize
    appendUInt16 builder "MinimumAllocation" header.MinimumAllocation
    appendUInt16 builder "MaximumAllocation" header.MaximumAllocation
    appendUInt16 builder "InitialSsValue" header.InitialSsValue
    appendUInt16 builder "InitialSpValue" header.InitialSpValue
    appendUInt16 builder "Checksum" header.Checksum
    appendUInt16 builder "InitialIpValue" header.InitialIpValue
    appendUInt16 builder "InitialCsValue" header.InitialCsValue
    appendUInt16 builder "FileAddressOfRelocationTable" header.FileAddressOfRelocationTable
    appendUInt16 builder "OverlayNumber" header.OverlayNumber
    appendUInt16List builder "Reserved1" header.Reserved1
    appendUInt16 builder "OemIdentifier" header.OemIdentifier
    appendUInt16 builder "OemInfo" header.OemInfo
    appendUInt16List builder "Reserved2" header.Reserved2
    appendUInt32 builder "FileAddressOfNewExeHeader" header.FileAddressOfNewExeHeader
    builder.AppendLine().ToString()