﻿open System

[<Flags>]
type MyEnum1 = A = 0b001 | B = 0b010 | C = 0b100

[<Flags>]
type MyEnum2 = X = 0b001 | Y = 0b010 | Z = 0b100

let test = 0b101

let printMyEnum<'a when 'a :> Enum> (myEnum : 'a) =
    for v in Enum.GetValues(typeof<'a>) do
        printfn "Flag: %A  Active: %b" v (myEnum.HasFlag(v :?> 'a))

printMyEnum (test |> enum<MyEnum1>)
printMyEnum (test |> enum<MyEnum2>)