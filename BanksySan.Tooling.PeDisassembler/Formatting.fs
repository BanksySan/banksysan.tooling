﻿module BanksySan.Tooling.PeDisassembler.Formatting

open System.Text
open System.IO
open System

[<Struct>]
type Value<'a> = {
    Value: 'a
    Offset: int64
}

let appendCharList (builder:StringBuilder) (name:string) (value:Value<char list>) =
        
    let offset = value.Offset
    let value = value.Value
    Printf.bprintf builder "0x%04X %-35s %A\n" offset name value
    
let appendUInt64 (builder:StringBuilder) (name:string) (value:Value<uint64>)  =
    let offset = value.Offset
    let value = value.Value
    Printf.bprintf builder "0x%04X %-35s 0x%016X\n" offset name value
    
let appendUInt32 (builder:StringBuilder) (name:string) (value:Value<uint32>)  =
    let offset = value.Offset
    let value = value.Value
    Printf.bprintf builder "0x%04X %-35s 0x%08X\n" offset name value
        
let appendUInt16 (builder:StringBuilder) (name:string) (value:Value<uint16>)  =
    let offset = value.Offset
    let value = value.Value
    Printf.bprintf builder "0x%04X %-35s 0x%04X\n" offset name value

let appendByte (builder:StringBuilder) (name:string) (value:Value<byte>)  =
    let offset = value.Offset
    let value = value.Value
    Printf.bprintf builder "0x%04X %-35s 0x%02X\n" offset name value

let appendUInt16List (builder:StringBuilder) (name:string) (value:Value<uint16> list)  =
    value |> List.iter (fun x -> Printf.bprintf builder  "0x%04X %-35s 0x%02X\n" x.Offset name x.Value)

let appendTimeDateStamp (builder:StringBuilder) (name:string) (value:Value<DateTime>) =
    let offset = value.Offset
    let value = value.Value.ToString("yyyy-MM-dd hh:mm:ss")
    Printf.bprintf builder "0x%04X %-35s %s\n" offset name value

let consumeBytes count (stream:Stream) =
    let offset = stream.Position
    let bytes:byte array = Array.zeroCreate count
    stream.Read(bytes, 0, count) |> ignore
    { Value = bytes |> List.ofArray ; Offset = offset}

let consumeChars count (stream:Stream)  =
    let bytes = consumeBytes count stream
    { 
        Value = bytes.Value |> List.map (fun x -> (char) x); 
        Offset = bytes.Offset 
    }

let consumeUInt64 (stream:Stream) =
    let offset = stream.Position
    let bytes:byte array = Array.zeroCreate 8
    stream.Read(bytes, 0, 8) |> ignore
    let value = BitConverter.ToUInt64(bytes, 0)
    { Value = value; Offset = offset }

let consumeUInt32 (stream:Stream) =
    let offset = stream.Position
    let bytes:byte array = Array.zeroCreate 4
    stream.Read(bytes, 0, 4) |> ignore
    let value = BitConverter.ToUInt32(bytes, 0)
    { Value = value; Offset = offset }

let consumeUInt16 (stream:Stream) =
    let offset = stream.Position
    let bytes:byte array = Array.zeroCreate 2
    stream.Read(bytes, 0, 2) |> ignore
    let value = BitConverter.ToUInt16(bytes, 0)
    { Value = value; Offset = offset }

let consumeByte (stream:Stream) =
    let offset = stream.Position
    let value = stream.ReadByte() |> byte
    {Value = value; Offset = offset}

let convertToDateTime (raw:Value<uint32>) =
    let epoch = new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)
    let dateTime = raw.Value |> Convert.ToDouble |> epoch.AddSeconds
    {
        Offset = raw.Offset
        Value = dateTime
    }