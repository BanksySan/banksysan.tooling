﻿module BanksySan.Tooling.PeDisassembler.Program 

open System
open System.IO
open System.Security
open System.Diagnostics
open BanksySan.Tooling.PeDisassembler.Coff.Optional

type FileExtractionReult =
| Success of Stream
| Failure of string

[<EntryPoint>]
let main argv =
    let openStream filePath =
        try
            let fileInfo = new FileInfo(filePath)
            match fileInfo.Exists with
            | true ->
                let stream = fileInfo.OpenRead() :> Stream
                Success stream
            | false ->
                let errorMessage =  sprintf "File '%s' doen't exist." fileInfo.FullName
                Failure errorMessage
        with 
            | :? ArgumentNullException 
            | :? SecurityException 
            | :? ArgumentException
            | :? UnauthorizedAccessException
            | :? PathTooLongException
            | :? NotSupportedException as e -> Failure e.Message

    let filePath =  match argv with
                    | [|filePathArg|] -> Some filePathArg
                    | _ -> None
    
    let exitCode = 
        if filePath.IsNone then 
            printf "Unvalid number of arguments: '%A'" argv
            1
        else
            let streamOpenResult = openStream filePath.Value
        
            match streamOpenResult with
            | Success stream -> 
                let dosHeader = BanksySan.Tooling.PeDisassembler.Dos.DosHeader.Parse stream
                stream.Position <- (int64) dosHeader.FileAddressOfNewExeHeader.Value
                let coffHeader =  BanksySan.Tooling.PeDisassembler.Coff.Header.Header.Parse stream
                let numberOfRvaAndSize = match coffHeader.OptionalHeader.WindowsSpecific with
                          | WindowsSpecificFields.Pe32 x -> x.NumberOfRvaAndSizes.Value
                          | WindowsSpecificFields.Pe32Plus x -> x.NumberOfRvaAndSizes.Value
                let numberOfSections = coffHeader.FileHeader.NumberOfSections.Value
                let sectionHeaders = BanksySan.Tooling.PeDisassembler.Coff.SectionHeaders.SectionHeaders.Parse numberOfSections numberOfRvaAndSize  stream

                let dosHeaderReport = dosHeader.Format 
                let coffReport = coffHeader.Format 
                let sectionReport = sectionHeaders.Format
                printf "%s" dosHeaderReport
                printf "%s" coffReport
                printf "%s" sectionReport
                0
            | Failure message -> 
                printf "Unable to open file stream:  %s" message
                2
#if DEBUG
    if Debugger.IsAttached then Console.ReadKey(true) |> ignore
#endif
    exitCode
