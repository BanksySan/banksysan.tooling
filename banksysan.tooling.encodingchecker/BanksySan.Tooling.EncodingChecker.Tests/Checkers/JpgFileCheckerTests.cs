namespace BanksySan.Tooling.EncodingChecker.Tests.Checkers
{
    using System.Threading.Tasks;
    using EncodingChecker.Checkers;
    using FileSystem;
    using Moq;
    using NUnit.Framework;

    public class JpgFileCheckerTests
    {
        private static readonly JpgFileChecker FILE_CHECKER = new JpgFileChecker();
        private static readonly int[] JPG_SIGNITURE = { 255, 216, 255, 224 };

        [Test]
        public async Task CorrectSigniture()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(255, 216, 255, 224, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.True);
        }

        [Test]
        public async Task IncorrectSignitureTooShort()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }

        [Test]
        public async Task IncorrectSignitureWrongBytes()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }
    }
}