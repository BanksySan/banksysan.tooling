﻿namespace BanksySan.Tooling.EncodingChecker.Tests.Checkers
{
    using System.Threading.Tasks;
    using EncodingChecker.Checkers;
    using FileSystem;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PngFileCheckerTests
    {
        private static readonly PngFileChecker CHECKER = new PngFileChecker();

        [Test]
        public async Task CorrectSigniture()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(137, 80, 78, 71, 13, 10, 26, 10, -1);
            var check = await CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.True);
        }

        [Test]
        public async Task IncorrectSignitureTooShort()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, -1);
            var check = await CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }

        [Test]
        public async Task IncorrectSignitureWrongBytes()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1);
            var check = await CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }
    }
}