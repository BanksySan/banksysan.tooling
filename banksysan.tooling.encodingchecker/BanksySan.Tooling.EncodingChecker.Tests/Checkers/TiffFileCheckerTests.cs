namespace BanksySan.Tooling.EncodingChecker.Tests.Checkers
{
    using System.Threading.Tasks;
    using EncodingChecker.Checkers;
    using FileSystem;
    using Moq;
    using NUnit.Framework;

    public class TiffFileCheckerTests
    {
        private static readonly TiffFileChecker FILE_CHECKER = new TiffFileChecker();
        private static readonly int[] TIFF_INTEL_SIGNATURE = { 73, 73, 42 };
        private static readonly int[] TIFF_MOTOROLA_SIGNATURE = { 77, 77, 42 };

        [Test]
        public async Task CorrectIntelSigniture()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(73, 73, 42, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.True);
        }

        [Test]
        public async Task CorrectMotorolaSigniture()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(77, 77, 42, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.True);
        }

        [Test]
        public async Task IncorrectSignitureTooShort()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }

        [Test]
        public async Task IncorrectSignitureWrongBytes()
        {
            var mockFileStream = new Mock<IFileStream>();
            var mockFile = new Mock<IFile>();

            mockFile.Setup(x => x.Open()).Returns(mockFileStream.Object);
            mockFileStream.Setup(x => x.ReadByte()).ReturnsInOrder(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1);
            var check = await FILE_CHECKER.Check(mockFile.Object);

            Assert.That(check, Is.False);
        }
    }
}