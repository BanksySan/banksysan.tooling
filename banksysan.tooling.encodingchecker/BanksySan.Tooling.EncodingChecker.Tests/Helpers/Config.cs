﻿namespace BanksySan.Tooling.EncodingChecker.Tests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    internal static class Config
    {
        static Config()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            var directoryName = Path.GetDirectoryName(path);
            if (directoryName == null) throw new DirectoryNotFoundException(path);
            var baseDirectory = new DirectoryInfo(directoryName);
            ImagesDirectory = baseDirectory.GetDirectories().Single(x => x.Name.Equals("Images", StringComparison.OrdinalIgnoreCase));
        }

        public static DirectoryInfo ImagesDirectory { get; }
    }
}