﻿namespace BanksySan.Tooling.EncodingChecker
{
    internal class AnalysisResult : IAnalysisResult
    {
        public string Status { get; set; }
        public string Details { get; set; }
        public string Path { get; set; }
    }
}