namespace BanksySan.Tooling.EncodingChecker.Checkers
{
    using System.Threading.Tasks;
    using FileSystem;

    public interface IFileChecker
    {
        Task<bool> Check(IFile file);
    }
}