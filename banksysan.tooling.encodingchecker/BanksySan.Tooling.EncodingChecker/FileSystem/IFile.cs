namespace BanksySan.Tooling.EncodingChecker.FileSystem
{
    public interface IFile
    {
        IFileStream Open();
    }
}