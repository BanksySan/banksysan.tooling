namespace BanksySan.Tooling.EncodingChecker.FileSystem
{
    using System;

    public interface IFileStream : IDisposable
    {
        int ReadByte();
    }
}