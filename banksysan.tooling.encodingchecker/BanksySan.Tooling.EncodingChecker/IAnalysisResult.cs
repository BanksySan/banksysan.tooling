﻿namespace BanksySan.Tooling.EncodingChecker
{
    public interface IAnalysisResult
    {
        string Status { get; }
        string Details { get; }

        string Path { get; }
    }
}