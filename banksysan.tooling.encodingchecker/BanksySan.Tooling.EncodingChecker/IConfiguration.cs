﻿namespace BanksySan.Tooling.EncodingChecker
{
    using System.IO;

    internal interface IConfiguration
    {
        DirectoryInfo BaseDirectory { get; }
    }
}