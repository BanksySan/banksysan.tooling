# BanksySan Encoding Checker #

## What is it? ##

Scans a directory tree, checking that file extensions are correct for the file type.

## Why bother? ##

A very good question.  I had a bug recently when I added the `X-Content-Type-Options` HTTP header.  Adding it broke a single image.  After some faffing about we realised that we actually had a GIF file posing as a PNG.

Assuming that if we had one such file then we probably had others, I wrote a wee program to walk the whole directory tree and confirm that each PNG was indeed a PNG and found a few more.

As I doubt that we're not the first to have this problem, I thought it would be nice to write this and make it able to check more than just PNG files.

## Explain how ##

Back in day, in the 1990s and early 2000s there were _The Browser Wars_.  Browsers were far more concerned with displaying as many web sites a possible than they were with trivial things like obeying standard and enforcing security.  They knew that if a website was badly written then the user would blame the browser (and switch browsers) rather than blame the web site owner.

One of the concessions made was how the browser will decide to handle a file it receives.  Intuition would dictate that it either uses the `Content-Type` type header and\or the extension of the file type?

In fact, it seems to do neither.  The browser will look at the file content and then decide for itself how to interpret and decode the file.  You can prove this.

1. Open a BMP, GIF, PNG or similar in the browser and it will display.
1. Change the file extension to a different image format's extension.
1. Open the file again.
1. The browser will _probably_ open it without and problems (I've tried this in Firefox and IE 11).

If you've got the energy, set up a little HTTP server ([Nancy](http://nancyfx.org/) or [Kestrel](https://github.com/aspnet/KestrelHttpServer) would be good candidates for this) or  or auto responder on [Fiddler](http://www.telerik.com/fiddler).

Now try sending various file formats with correct and incorrect `Content-Type` headers and correct and incorrect file extensions and you'll see that the browser will just render them without moaning.

## Why is this bad? ##

Security.  An attack called _MIME Type Confusion_ which allows an attacker to execute a malicious script on the victim's browser.  I won't try to explain it, but you can find details on the [Internet](https://www.google.co.uk/search?q="MIME+type+confusion").

[OWASP](https://www.owasp.org/index.php/OWASP_Secure_Headers_Project#X-Content-Type-Options) is a good starting place.